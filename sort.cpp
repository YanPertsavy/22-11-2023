﻿using namespace std;

#include <iostream>

void sort(int arr[],int n);

int main()
{
	const int n = 6;
	int arr[n]{ 78,17,45,62,9,12 };
	sort(arr,n);
	for (size_t i = 0; i < n; i++)
	{
		cout << arr[i]<<" ";
	}
}
void sort(int arr[],int n)
{
	for (size_t i = 1; i < n; i++) {
		for (size_t j = 0; j < n - i; j++) {
			if (arr[j] > arr[j + 1]) {
				int temp = arr[j];
				arr[j] = arr[j + 1];
				arr[j + 1] = temp;
			}
		}
	}
	
}
