﻿using namespace std;

#include <iostream>

void selectionSort(int arr[], int n);

int main() {
    int arr[] = { 67, 21, 17, 26, 2 };
    const int n = 5;
    selectionSort(arr, n);
    cout << "Sorted array: ";
    for (int i = 0; i < n; ++i) {
        cout << arr[i] << " ";
    }
    
}
void selectionSort(int arr[], int n)
{
    for (int i = 0; i < n - 1; ++i) {
        int minIndex = i;
        for (int j = i + 1; j < n; ++j) {
            if (arr[j] < arr[minIndex]) {
                minIndex = j;
            }
        }
        if (minIndex != i) {
            int tmp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = tmp;

        }
    }
}

